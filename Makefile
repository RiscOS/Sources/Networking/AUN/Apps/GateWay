# Copyright 1995 Acorn Computers Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Makefile for GateWay
#

COMPONENT  = GateWay
TARGET     = !RunImage
INSTTYPE   = app
OBJS       = wimp
LIBS       = ${RLIB}
CINCLUDES  = ${TCPIPINC}
CFLAGS     = ${C_NOWARN_NON_ANSI_INCLUDES}
INSTAPP_DEPENDS = Resources${SEP}SetGtWay
INSTAPP_FILES = !Boot !Run !RunImage !Sprites !Sprites22 !Help Messages Templates !Configure\
         Configure:Files MAP:Files StartImage:Resources SetGtWay:Resources
INSTAPP_VERSION = Messages

include CApp

clean::
	${RM} Resources${SEP}SetGtWay

Resources${SEP}SetGtWay: main.o setgtway.o
	${LD} -o $@ $? ${CLIB} ${NET5LIBS}
	${SQZ} $@

# Dynamic dependencies:
